*** Settings ***
Library     AppiumLibrary

*** Test Cases ***
Verify that can open app and navigate
    ${capability}       Create dictionary          automationName=XCUItest
    ...                 platformName=ios
    ...                 platformVersion=15.5
    ...                 bundleId=com.nsoojin.BookStore
    ...                 deviceName=iPhone 12 Pro Max Test
    Open Application    remote_url=http://0.0.0.0:4723/wd/hub
    ...                 &{capability}
    Wait until element is visible           id=Search
    Tap             id=Search
    Wait until element is visible           xpath=//XCUIElementTypeSearchField[@name="Search"]
    Input text          xpath=//XCUIElementTypeSearchField[@name="Search"]          MySQL
    Wait until element is visible           xpath=//XCUIElementTypeTable[@name="SearchTableView"]/XCUIElementTypeCell[1]
    Tap                 xpath=//XCUIElementTypeTable[@name="SearchTableView"]/XCUIElementTypeCell[1]
    Sleep       2s
    Scroll Down                             xpath=//XCUIElementTypeStaticText[@name="Details"]
    Sleep       2s
    Scroll Up                               xpath=(//XCUIElementTypeStaticText[@name="MySQL Stored Procedure Programming"])[2]
    Wait until element is visible           chain=**/XCUIElementTypeButton[`name == "Close"`]    
    Tap             chain=**/XCUIElementTypeButton[`name == "Close"`]    
    Wait until element is visible           xpath=//XCUIElementTypeTable[@name="SearchTableView"]/XCUIElementTypeCell[//XCUIElementTypeStaticText[@name="MySQL Stored Procedure Programming"] and //XCUIElementTypeStaticText[@name="$6.38"]]